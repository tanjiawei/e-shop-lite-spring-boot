package com.westleytan.eshoplite.service;

import java.util.Set;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.westleytan.eshoplite.dao.CustomerRepository;
import com.westleytan.eshoplite.dto.Purchase;
import com.westleytan.eshoplite.dto.PurchaseResponse;
import com.westleytan.eshoplite.entity.Customer;
import com.westleytan.eshoplite.entity.Order;
import com.westleytan.eshoplite.entity.OrderItem;

@Service
public class CheckoutServiceImpl implements CheckoutService {

	private CustomerRepository customerRepository;

//	@Autowired
	public CheckoutServiceImpl(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	@Transactional
	public PurchaseResponse placeOrder(Purchase purchase) {
//
		Order order = purchase.getOrder();

		String orderTrackingNumber = generateOrderTrackingNumber();

		order.setOrderTrackingNumber(orderTrackingNumber);

		Set<OrderItem> orderItems = purchase.getOrderItems();
		orderItems.forEach(order::addItemToOrder);

		order.setBillingAddress(purchase.getBillingAddress());
		order.setShippingAddress(purchase.getShippingAddress());

		Customer customer = purchase.getCustomer();
		String email = customer.getEmail();
		Customer customerFromDB = customerRepository.findByEmail(email);

		if (customerFromDB != null)
			customer = customerFromDB;

		customer.addOrderToCustomer(order);

		customerRepository.save(customer);
//		private String status;

		return new PurchaseResponse(orderTrackingNumber);
	}

	private String generateOrderTrackingNumber() {
		return UUID.randomUUID().toString();
	}

}
