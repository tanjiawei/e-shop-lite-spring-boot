package com.westleytan.eshoplite.service;

import com.westleytan.eshoplite.dto.Purchase;
import com.westleytan.eshoplite.dto.PurchaseResponse;

public interface CheckoutService {
	PurchaseResponse placeOrder(Purchase purchase);
}
