package com.westleytan.eshoplite.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;

import com.westleytan.eshoplite.entity.Country;
import com.westleytan.eshoplite.entity.Order;
import com.westleytan.eshoplite.entity.Product;
import com.westleytan.eshoplite.entity.ProductCategory;
import com.westleytan.eshoplite.entity.State;

@Configuration
public class DataRestConfig implements RepositoryRestConfigurer {

	@Autowired
	private EntityManager entityManager;

	@Value("${allowed.origins}")
	private String allowedOrigins;

	@Value("${server.servlet.context-path}")
	private String basePath;

	@Override
	public void configureRepositoryRestConfiguration(
			RepositoryRestConfiguration config) {

		HttpMethod[] unsupportedHttpMethods = { HttpMethod.DELETE,
				HttpMethod.PUT, HttpMethod.POST, HttpMethod.PATCH };

		// disable HTTP methods

		@SuppressWarnings("rawtypes")
		Class[] classes = new Class[] { Product.class, ProductCategory.class,
				Country.class, State.class, Order.class };
		Arrays.asList(classes)
				.forEach(className -> disableHttpMethods(className, config,
						unsupportedHttpMethods));

		// expose the id for entity
		List<Object> entityClasses = new ArrayList<>();

		entityManager.getMetamodel().getEntities().stream()
				.map(e -> e.getJavaType()).forEach(entityClasses::add);

		@SuppressWarnings("rawtypes")
		Class[] domainTypes = entityClasses.toArray(new Class[0]);

		config.exposeIdsFor(domainTypes);

	}

	private void disableHttpMethods(Class<?> entityClass,
			RepositoryRestConfiguration config,
			HttpMethod[] unsupportedHttpMethods) {
		config.getExposureConfiguration().forDomainType(entityClass)
				.withItemExposure((metadata, httpMethods) -> httpMethods
						.disable(unsupportedHttpMethods))
				.withCollectionExposure((metadata, httpMethods) -> httpMethods
						.disable(unsupportedHttpMethods));
	}

}
