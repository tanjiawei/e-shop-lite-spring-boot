package com.westleytan.eshoplite.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.okta.spring.boot.oauth.Okta;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/orders/**").authenticated().and()
				.oauth2ResourceServer().jwt();

		http.cors();

		Okta.configureResourceServer401ResponseBody(http);

		// temporarily disable CSRF for cookies session tracking
		http.csrf().disable();
	}

}
