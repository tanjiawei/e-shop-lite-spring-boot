package com.westleytan.eshoplite.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.westleytan.eshoplite.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	Customer findByEmail(String email);
}
