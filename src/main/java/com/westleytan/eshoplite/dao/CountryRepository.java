package com.westleytan.eshoplite.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.westleytan.eshoplite.entity.Country;

@CrossOrigin
@RepositoryRestResource
public interface CountryRepository extends JpaRepository<Country, Integer> {

}
