package com.westleytan.eshoplite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EShopLiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(EShopLiteApplication.class, args);
	}

}
