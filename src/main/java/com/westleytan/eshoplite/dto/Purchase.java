package com.westleytan.eshoplite.dto;

import java.util.HashSet;
import java.util.Set;

import com.westleytan.eshoplite.entity.Address;
import com.westleytan.eshoplite.entity.Customer;
import com.westleytan.eshoplite.entity.Order;
import com.westleytan.eshoplite.entity.OrderItem;

import lombok.Data;

@Data
public class Purchase {

	private Customer customer;
	private Address shippingAddress;
	private Address billingAddress;
	private Order order;
	private Set<OrderItem> orderItems = new HashSet<>();

}
